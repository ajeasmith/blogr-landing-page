const menu_bars = document.querySelector('.fa-bars');
const menu_times = document.querySelector('.fa-times');
const menu_mobile = document.querySelector('.menu_mobile');
const mobile_links = document.querySelectorAll('.link');
const desktop_links = document.querySelectorAll('.desktop_link');




// Mobile Menu Functionality
menu_bars.addEventListener('click', () => {
    menu_bars.classList.remove('show');
    menu_bars.classList.add('hide');

    menu_times.classList.remove('hide');
    menu_times.classList.add('show');

    if (menu_times.classList.contains('show')) {
        menu_mobile.classList.add('show')
        menu_mobile.classList.remove('hide');
    }
})
menu_times.addEventListener('click', () => {

    menu_times.classList.remove('show');
    menu_times.classList.add('hide');

    menu_bars.classList.remove('hide');
    menu_bars.classList.add('show');

    if (menu_bars.classList.contains('show')) {
        menu_mobile.classList.remove('show');
        menu_mobile.classList.add('hide');
    }
    if (menu_bars.classList.contains('show')) {
        mobile_links.forEach(link => {
            link.nextElementSibling.classList.remove('show')
        })
    }

})
mobile_links.forEach((link) => {
    link.addEventListener('click', () => {
        link.nextElementSibling.classList.toggle('show')
    })
})

// Desktop Menu Functionality
desktop_links.forEach(link => {
    link.addEventListener('click', () => {
        link.nextElementSibling.classList.toggle('hide')
    })
})